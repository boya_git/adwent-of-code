class Display:

    def __init__(self, filename):
        self.row_data = self.load_data(filename)

    @staticmethod
    def load_data(filename):
        with open(filename, 'r') as file:
            return [line.replace('\n', '') for line in file.readlines()]

    def first(self):
        total = sum([1
                     for line in self.row_data
                     for s in line.split('|')[1].split()
                     if len(s) in (2, 4, 3, 7)])
        return total

    def decode(self, row_line):
        ct = {}
        row = row_line.split('|')[0].split()
        for word in row:
            if len(word) == 2:
                ct[1] = set(word)
            elif len(word) == 4:
                ct[4] = set(word)
            elif len(word) == 3:
                ct[7] = set(word)
            elif len(word) == 7:
                ct[8] = set(word)
        for word in row:
            if len(word) == 5 and len(set(word).difference(ct[1])) == 3:
                ct[3] = set(word)
        ct[9] = ct[3].union(ct[4].difference(ct[1]))
        # (Need to find best way)
        ct[0] = ct[8].difference(ct[4].difference(ct[1]).difference(ct[4].difference(ct[3])))
        for word in row:
            if len(word) == 6 and set(word) not in ct.values():
                ct[6] = set(word)
        for word in row:
            if len(word) == 5 and len(set(word).difference(ct[4])) == 3:
                ct[2] = set(word)
        for word in row:
            if set(word) not in ct.values():
                ct[5] = set(word)
        return ct

    def second(self):
        total = 0
        for line in self.row_data:
            decrypted = []
            ct = self.decode(line)
            line = line.split('|')[1].split()
            for word in line:
                for key, value in ct.items():
                    if set(word) == value:
                        decrypted.append(str(key))
            total += int(''.join(decrypted))
        return total


d = Display('puzzle_input.txt')
print(d.first())
print(d.second())
