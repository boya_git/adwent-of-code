class CrabCluster:

    def __init__(self, filename):
        self.crabs = self.load_data(filename)

    @staticmethod
    def load_data(filename):
        with open(filename, 'r') as file:
            return list(map(int, file.read().split(',')))

    def pos(self):
        s = sorted(self.crabs)
        l = len(self.crabs)
        return int((s[int((l/2)-1)]+s[int(l/2)])/2)

    @staticmethod
    def fuel_consumption(c, k):
        count = sum(range(int(abs(c - k)) + 1))
        return count

    # 1st part
    def first(self):
        k = self.pos()
        fuel = sum([abs(c - k) for c in self.crabs])
        return fuel

    # 2nd part
    def second(self):
        fuels = []
        for i in range(max(self.crabs)):
            fuels.append(sum([self.fuel_consumption(j, i) for j in self.crabs]))
        return min(fuels)


cc = CrabCluster('puzzle_input.txt')
print(cc.first())
print(cc.second())
