def frequent_in_column(n, array, reverse=False) -> int:
    """
        Return most frequent element in the selected column in report
    :param n: selected column
    :param array: report
    :param reverse: for reversed output
    """
    if reverse:
        return 0 if sum([int(line[n]) for line in array]) >= len(array) / 2 else 1
    return 1 if sum([int(line[n]) for line in array]) >= len(array)/2 else 0


def rating(array, reverse=False) -> int:
    """
        Return the decimal value of oxygen generator rating and CO2 scrubber rating.
    :param array: report
    :param reverse: for reversed output
    """
    tmp = array
    for i in range(len(array[0])):
        if len(tmp) == 1:
            break
        tmp = list(filter(lambda line: True if int(line[i]) == frequent_in_column(i, tmp, reverse=reverse) else False, tmp))
    tmp = tmp[0]
    return int(''.join(tmp), 2)


def diagnostic(filename) -> tuple:
    with open(filename, 'r') as file:
        report = list(map(lambda line: list(str(line).replace('\n', '')), file.readlines()))

    # 1st part
    gamma = []
    for i in range(len(report[0])):
        gamma.append(frequent_in_column(i, report))
    epsilon = [0 if bit else 1 for bit in gamma]

    gamma_k = int(''.join(map(str, gamma)), 2)
    epsilon_k = int(''.join(map(str, epsilon)), 2)

    # 2nd part
    oxygen_k = rating(report, reverse=False)
    co2_k = rating(report, reverse=True)

    return (gamma_k*epsilon_k, oxygen_k*co2_k)


print(diagnostic('puzzle_data.txt')) # => (3687446, 4406844)
