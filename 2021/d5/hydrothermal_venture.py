from PIL import Image
import numpy as np


# Settings
FILENAME = 'puzzle_data.txt'


def load_data(filename: str) -> list:
    with open(filename, 'r') as file:
        data = [line.replace('\n', '').split(' -> ') for line in file.readlines()]
    return [[list(map(int, line[0].split(','))), list(map(int, line[1].split(',')))]
            for line in data]


def draw_line(line: list):
    x1, y1 = line[0][0], line[0][1]
    x2, y2 = line[1][0], line[1][1]
    if x1 == x2:
        if y1 <= y2:
            for j in range(y1, y2+1):
                MAP[j][x1] += 1
        else:
            for j in range(y1, y2-1, -1):
                MAP[j][x1] += 1

    elif y1 == y2:
        if x1 <= x2:
            for i in range(x1, x2+1):
                MAP[y1][i] += 1
        else:
            for i in range(x1, x2-1, -1):
                MAP[y1][i] += 1
    else:
        if x1 < x2 and y1 < y2:
            for j, i in list(zip(list(range(x1, x2+1)), list(range(y1, y2+1)))):
                MAP[i][j] += 1
        if x1 < x2 and y1 > y2:
            for j, i in list(zip(list(range(x1, x2+1)), list(range(y1, y2-1, -1)))):
                MAP[i][j] += 1
        elif x1 > x2 and y1 > y2:
            for j, i in list(zip(list(range(x1, x2-1, -1)), list(range(y1, y2-1, -1)))):
                MAP[i][j] += 1
        elif x1 > x2 and y1 < y2:
            for j, i in list(zip(list(range(x1, x2-1, -1)), list(range(y1, y2+1)))):
                MAP[i][j] += 1


def find_zones() -> int:
    counter = 0
    for i in range(SIZE):
        for j in range(SIZE):
            if MAP[i][j] >= 2:
                counter += 1
    return counter


def hydrothermal_map():
    for line in DATA:
        draw_line(line)
    print(find_zones())

    # Not necessary, but funny
    # Create vents map image
    k = 255 // max([max(line) for line in MAP])
    pixels = [[[pixel*k, pixel*k, pixel*k] for pixel in line] for line in MAP]
    pixels_array = np.array(pixels, dtype=np.uint8)
    vents_map = Image.fromarray(pixels_array)
    vents_map.save('vents_map.png')


if __name__ == '__main__':
    # Globals
    DATA = load_data(FILENAME)
    SIZE = max([c for line in DATA for point in line for c in point]) + 1
    MAP = [[0 for _ in range(SIZE)] for _ in range(SIZE)]
    hydrothermal_map()
