class SchoolOfFish:
    def load_fishes(self, filename):
        with open(filename, 'r') as file:
            self.init_fishes = list(map(int, file.read().split(',')))

    def live(self, days):
        old = [self.init_fishes.count(i) for i in range(9)]

        for _ in range(days):
            new = [0 for _ in range(9)]
            for i in reversed(range(9)):
                new[i-1] = old[i]

            new[6] += old[0]
            old = new.copy()

        return sum(old)


sof = SchoolOfFish()
sof.load_fishes('puzzle_input.txt')
print(sof.live(256))
