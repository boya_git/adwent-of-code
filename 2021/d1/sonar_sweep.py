# 2nd part
def measurement(filename):
    with open(filename, 'r') as file:
        report = list(map(int, file.readlines()))

    counter = 0
    report_sw = []
    i = 0
    while i < len(report)-2:
        report_sw.append(report[i]+report[i+1]+report[i+2])
        i += 1

    prev = report_sw[0]
    for x in report_sw[1:]:
        if x > prev:
            counter += 1
        prev = x
    return counter


print(measurement('puzzle_data.txt')) # => 1589
