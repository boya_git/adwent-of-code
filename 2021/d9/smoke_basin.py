class Cave:

    def __init__(self, filename):
        self.area = self.load_data(filename)

    def is_lowest(self, i, j):
        compare = []
        if i-1 >= 0:
            compare.append(self.area[i][j] < self.area[i-1][j])
        if i+1 <= len(self.area)-1:
            compare.append(self.area[i][j] < self.area[i+1][j])
        if j-1 >= 0:
            compare.append(self.area[i][j] < self.area[i][j-1])
        if j+1 <= len(self.area[i])-1:
            compare.append(self.area[i][j] < self.area[i][j+1])
        return all(compare)

    @staticmethod
    def load_data(filename):
        data = []
        with open(filename, 'r') as file:
            for line in file.readlines():
                data.append([int(i) for i in line if i != '\n'])
        return data

    def first(self):
        result = 0
        for i in range(len(self.area)):
            for j in range(len(self.area[0])):
                if self.is_lowest(i, j):
                    result += 1 + self.area[i][j]
        return result



cave = Cave('puzzle_input.txt')
print(cave.first())
