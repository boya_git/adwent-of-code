# 2nd part
class Submarine:
    def __init__(self):
        self.h = 0
        self.d = 0
        self.aim = 0

    def follow_the_course(self, filename):
        with open(filename, 'r') as file:
            for line in file:
                action, value = line.split()
                getattr(self, action)(int(value))

    def forward(self, n):
        self.h += n
        self.d += self.aim * n

    def down(self, n):
        self.aim += n

    def up(self, n):
        self.aim -= n

    def current_pos(self):
        print(f'Horizontal: {self.h}\n'
              f'Depth: {self.d}\n'
              f'Aim: {self.aim}')

    def answer(self):
        print(f'Answer: {self.h * self.d}')


s = Submarine()
s.follow_the_course('puzzle_data.txt')
s.answer() # => Answer: 1960569556