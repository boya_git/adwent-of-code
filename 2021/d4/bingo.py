def load_data(filename):
    with open(filename, 'r') as f:
        file_data = f.readlines()

    sequence = list(map(int, file_data[0].replace('\n', '').split(',')))
    boards = []
    shadows = []
    tmp = []
    size = len(file_data[2:][0].replace('\n', '').split())
    for row in [row.replace('\n', '') for row in file_data[2:]]:
        if row:
            tmp.append(row)
        else:
            boards.append([list(map(int, line.split())) for line in tmp])
            shadows.append([[0, 0, 0, 0, 0] for _ in range(size)])
            tmp = []
    boards.append([list(map(int, line.split())) for line in tmp])
    shadows.append([[0, 0, 0, 0, 0] for _ in range(size)])
    return sequence, boards, shadows, size


def is_board_win(shadow):
    for line in shadow:
        if all(line):
            return True

    for i in range(len(shadow)):
        if all([line[i] for line in shadow]):
            return True
    return False


def process_shadow(n, board, shadow, size):
    for i in range(size):
        for j in range(size):
            if board[i][j] == n:
                shadow[i][j] = 1
    return shadow


def calculate_score(board, shadow, size, n):
    score = 0
    for x in range(size):
        for y in range(size):
            if shadow[x][y] == 0:
                score += board[x][y]
    return score*n


def bingo(data_file):
    sequence, boards, shadows, size = load_data(data_file)
    first = {
        'id': 0,
        'board': [],
        'shadow': [],
        'number': 0,
    }
    last = {
        'id': 0,
        'board': [],
        'shadow': [],
        'number': 0,
    }
    boards_win = []
    for n in sequence:
        for i in range(len(boards)):
            if i not in boards_win:
                shadows[i] = process_shadow(n, boards[i], shadows[i], size)
                if is_board_win(shadows[i]):
                    if not first['id']:
                        first['id'] = i
                        first['board'] = boards[i]
                        first['shadow'] = shadows[i]
                        first['number'] = n
                    boards_win.append(i)
                    last['id'] = i
                    last['board'] = boards[i]
                    last['shadow'] = shadows[i]
                    last['number'] = n

    return [calculate_score(first['board'], first['shadow'], size, first['number']),
            calculate_score(last['board'], last['shadow'], size, last['number'])]


if __name__ == '__main__':
    print(bingo('puzzle_data.txt'))
